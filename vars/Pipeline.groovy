def call() {
    node {
        stage('checkout')
        {
        checkout scm: [$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[url: 'https://gitlab.com/adityaroy/spring3.git']]]
        }
        def p = pipelineConfig()
        stage('build artifact') {
                sh 'mvn clean install'
            }
       
        stage('copy artifact to workspace') {
                sh ' cp ${WORKSPACE}/target/Spring3HibernateApp.war ${WORKSPACE}/'
            }
        stage('build image') {
                sh "sudo docker build -t ${p.IMAGE_NAME} ."
                }
            
            stage('Deploy') { 
                sh "sudo docker run -p ${p.NODE_PORT}:${p.CONTAINER_PORT} -d ${p.IMAGE_NAME}"
                }
            
    }
}